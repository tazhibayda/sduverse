package auth

import (
	"cloud.google.com/go/storage"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"net/http"
	"sduverse/internal/models"
	"time"
)

type AuthRepository struct {
	repository *mongo.Collection
	storage    *storage.Client
	Redis      *redis.Client
	Logger     *logrus.Logger
	SecretKey  string
}

func NewAuthRepository(repository *mongo.Collection, client *storage.Client, Redis *redis.Client, Logger *logrus.Logger, SecretKey string) *AuthRepository {
	return &AuthRepository{repository: repository, storage: client, Redis: Redis, Logger: Logger, SecretKey: SecretKey}
}

//var secretKey = []byte("secret-key")

func (repository *AuthRepository) createToken(username string) (string, error) {
	claims := jwt.MapClaims{
		"username": username,
		"exp":      time.Now().Add(time.Hour * 24).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(repository.SecretKey))
}

// SignUp @Summary Sign up a new user
// @Description Register a new user
// @Accept json
// @Tags Auth
// @Produce json
// @Param input body models.User true "User details"
// @Success 201 {object} models.User
// @Failure 400 {object} error
// @Failure 409 {object} error
// @Router /auth/signup [post]
func (repository *AuthRepository) SignUp(c *gin.Context) {
	var request models.User
	//userRepository := handlers.UserRepository{}
	if c.BindJSON(&request) != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})
		return
	}
	hashPassword, err := HashPassword(request.Password)

	if err != nil {
		c.JSON(http.StatusConflict, gin.H{"error": "hashing password error"})
	}
	request.Password = hashPassword
	request.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	//request.Create()
	request.ID_ = primitive.NewObjectID()
	_, err = repository.repository.InsertOne(context.Background(), request)
	if err != nil {
		fmt.Errorf("create user error")
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"user": request,
	})
}

type LoginRequest struct {
	Username string
	Password string
}

// Login @Summary Login
// @Description Login with username and password
// @Accept json
// @Produce json
// @Tags Auth
// @Param input body LoginRequest true "Username and password"
// @Success 200 {object} error
// @Failure 400 {object} error
// @Failure 401 {object} error
// @Router /auth/login [post]
func (repository *AuthRepository) Login(c *gin.Context) {
	var loginRequest LoginRequest
	if c.BindJSON(&loginRequest) != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to read body",
		})

		return
	}
	var user models.User
	//userCollection := utils.DB.Collection("users")
	err := repository.repository.FindOne(context.Background(), bson.M{"username": loginRequest.Username}).Decode(&user)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid email or password"})
		return
	}

	if !CheckPasswordHash(loginRequest.Password, user.Password) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid email or password "})
		return
	}

	token, err := repository.createToken(user.Username)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Failed to create token",
		})
		return
	}

	refreshToken, err := generateUUID()
	if err != nil {
		repository.Logger.Info("Could not generate refresh token")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Could not generate refresh token"})
		return
	}
	http.SetCookie(c.Writer, &http.Cookie{
		Name:     "refresh_token",
		Value:    refreshToken,
		HttpOnly: true,
		Path:     "/auth",
		MaxAge:   int(7 * 24 * 60 * 60), // 7 дней
	})
	c.SetSameSite(http.SameSiteLaxMode)
	//c.SetCookie("Authorization", "Bearer "+token, 3600*24*30, "", "", false, true)
	//c.SetCookie( )

	err = repository.CreateRefreshSession(user, refreshToken, c.Request.UserAgent(), c.ClientIP(), 7*24*60*60)
	if err != nil {
		repository.Logger.Info("Failed to store refresh session")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to store refresh session"})
		return
	}
	c.Writer.Header().Set("Authorization", "Bearer "+token)
	//c.Request.Header["Authorization"][0] = "Bearer " + token
	c.JSON(http.StatusOK, gin.H{
		"access_token":  token,
		"refresh_token": refreshToken,
		"message":       user,
	})
}

func (repository *AuthRepository) LogoutUser(c *gin.Context) {
	refreshToken, err := c.Cookie("refresh_token")
	if err != nil {
		repository.Logger.Error("No refresh token")
		c.JSON(http.StatusBadRequest, gin.H{"error": "No refresh token"})
		return
	}
	sessionKey := fmt.Sprintf("refresh_token:%s", refreshToken)
	_, err = repository.Redis.Del(context.Background(), sessionKey).Result()
	if err != nil {
		repository.Logger.Error("Failed to logout")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to logout"})
		return
	}

	http.SetCookie(c.Writer, &http.Cookie{
		Name:    "refresh_token",
		Path:    "/auth",
		Expires: time.Now(),
	})
	repository.Logger.Info("Logout successful")
	c.JSON(http.StatusOK, gin.H{"message": "Logout successful"})
}

func (repository *AuthRepository) CreateRefreshSession(user models.User, token, userAgent, ip string, expiresIn int64) error {
	sessionKey := fmt.Sprintf("refresh_token:%s", token)

	sessionData := map[string]interface{}{
		"userId":    user.ID_.Hex(),
		"username":  user.Username,
		"userAgent": userAgent,
		"ip":        ip,
		"createdAt": time.Now().Unix(),
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := repository.Redis.HMSet(ctx, sessionKey, sessionData).Err()
	if err != nil {
		fmt.Println(err.Error())
		repository.Logger.Error("Failed to fetch data from Redis")
		return err
	}

	repository.Logger.Info("Successfully created refresh session")
	return repository.Redis.Expire(ctx, sessionKey, time.Duration(expiresIn)*time.Second).Err()
	//return nil
}

func (repository *AuthRepository) RefreshToken(c *gin.Context) {
	refreshToken, err := c.Cookie("refresh_token")
	if err != nil {
		repository.Logger.Info("No refresh token")
		c.JSON(http.StatusUnauthorized, gin.H{"error": "No refresh token"})
		return
	}

	// Получаем данные сессии по токену из Redis
	sessionKey := fmt.Sprintf("refresh_token:%s", refreshToken)
	sessionData, err := repository.Redis.HGetAll(context.Background(), sessionKey).Result()
	if err != nil || len(sessionData) == 0 {
		repository.Logger.Info("Invalid refresh session")
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid refresh session"})
		return
	}
	// Проверяем время жизни и соответствие fingerprint
	ttl := repository.Redis.TTL(context.Background(), sessionKey)
	if 0 >= ttl.Val().Seconds() {
		repository.Logger.Info("Expired or invalid refresh session")
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Expired or invalid refresh session"})
		return
	}
	newAccessToken, err := repository.createToken(sessionData["username"])
	if err != nil {
		repository.Logger.Info("Failed to create new access token")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create new access token"})
		return
	}
	repository.Logger.Info("Token refreshed successfully")
	c.JSON(http.StatusOK, gin.H{"access_token": newAccessToken, "refresh_token": refreshToken})
}

func generateUUID() (string, error) {
	uuid, err := uuid.NewRandom()
	if err != nil {
		return "", err
	}
	return uuid.String(), nil
}
