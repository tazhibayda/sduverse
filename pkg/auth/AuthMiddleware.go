package auth

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"golang.org/x/net/context"
	"net/http"
	//"sduverse/pkg/auth"
)

func (repository AuthRepository) AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" {
			repository.Logger.Error("Authorization header is required")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header is required"})
			c.Abort()
			return
		}

		tokenString = tokenString[len("Bearer "):]

		sessionKey := fmt.Sprintf("revoked_access_token:%s", tokenString)
		revoked, err := repository.Redis.Exists(context.Background(), sessionKey).Result()
		if err == nil && revoked > 0 {
			repository.Logger.Error("Token has been revoked")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Token has been revoked"})
			c.Abort()
			return
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				repository.Logger.Error("unexpected signing method")
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(repository.SecretKey), nil
		})

		if err != nil || !token.Valid {
			repository.Logger.Error("Invalid token")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			c.Abort()
			return
		}

		repository.Logger.Info("Authorization middleware successfully")
		c.Next()
	}
}
