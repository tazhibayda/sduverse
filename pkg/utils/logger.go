package utils

import (
	"github.com/sirupsen/logrus"
	"os"
)

func Init() logrus.Logger {
	var logger logrus.Logger
	// Log as JSON instead of the default ASCII formatter.
	logger.SetFormatter(&logrus.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logger.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logger.SetLevel(logrus.InfoLevel)

	return logger
}

