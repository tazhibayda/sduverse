package utils

import "github.com/gin-gonic/gin"

var Router *gin.Engine

func SetupRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.ReleaseMode)
	return router
}
