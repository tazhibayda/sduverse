package utils

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/net/context"
	"log"
	"sduverse/config"
)

var client *mongo.Client

func DBSetup(config config.Config) *mongo.Database {
	client, err := mongo.NewClient(options.Client().ApplyURI(config.Host))
	if err != nil {
		log.Fatal(err)
	}

	//credential := options.Credential{
	//	Username: config.Username,
	//	Password: config.Password,
	//}

	clientOpts := options.Client().ApplyURI(config.Host)
	//.SetAuth(credential)
	client, err = mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		log.Fatal(err)
	}

	db := client.Database(config.Database)

	return db
}

func InitializeDB(client *mongo.Client, logger *logrus.Logger) {
	collections := []string{"users", "posts", "comments", "services", "books", "housing", "lostandfound"}
	for _, coll := range collections {
		if err := createCollectionIfNotExists(client, "SDUVerse", coll, logger); err != nil {
			//log.Fatal().Str("func", "ConnectDB").Err(err).Msg("Failed to create collection")
			logger.Errorf("Failed to create collection %s", coll)
		}
	}
}

func createCollectionIfNotExists(client *mongo.Client, dbName, collectionName string, logger *logrus.Logger) error {
	exists, err := collExists(client, dbName, collectionName)
	if err != nil {
		return err
	}
	if !exists {
		logger.Info(fmt.Sprintf("Creating collection: %s\n", collectionName))
		return client.Database(dbName).CreateCollection(context.Background(), collectionName)
	}
	return nil
}

func collExists(client *mongo.Client, dbName, collectionName string) (bool, error) {
	collections, err := client.Database(dbName).ListCollectionNames(context.Background(), bson.M{})
	if err != nil {
		return false, err
	}
	for _, coll := range collections {
		if coll == collectionName {
			return true, nil
		}
	}
	return false, nil
}

func DisconnectDB() {
	err := client.Disconnect(context.Background())
	if err != nil {
		log.Fatal("Disconnect from database failed: " + err.Error())
	}
}
