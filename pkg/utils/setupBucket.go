package utils

import (
	"cloud.google.com/go/storage"
	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"google.golang.org/appengine"
	"io"
	"net/http"
	"net/url"
)

var (
	storageClient *storage.Client
)

func InitStorage(storageJSONName string) *storage.Client {
	//storageClient :=
	storageClient, err := storage.NewClient(context.Background(), option.WithCredentialsFile(storageJSONName))
	if err != nil {
		return nil
	}
	return storageClient
}

func HandleFileUploadToBucket(c *gin.Context) {
	bucket := "verse_1" //your bucket name

	var err error

	ctx := appengine.NewContext(c.Request)

	storageClient, err = storage.NewClient(ctx, option.WithCredentialsFile("sduverse-04ef4993e58d.json"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	defer f.Close()

	sw := storageClient.Bucket(bucket).Object(uploadedFile.Filename).NewWriter(ctx)

	if _, err := io.Copy(sw, f); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	if err := sw.Close(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"Error":   true,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message":  "file uploaded successfully",
		"pathname": u.EscapedPath(),
	})
}

func HandleFileDeleteFromBucket(c *gin.Context) {
	bucket := "verse_1"             // your bucket name
	filename := c.Param("filename") // assuming the filename is passed as a URL parameter

	ctx := appengine.NewContext(c.Request)

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile("sduverse-04ef4993e58d.json"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}
	defer storageClient.Close()

	objectHandle := storageClient.Bucket(bucket).Object(filename)
	if err := objectHandle.Delete(ctx); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "file deleted successfully",
	})
}

func HandleFileUpdateInBucket(c *gin.Context) {
	bucket := "verse_1" // your bucket name
	//filename := c.Param("filename") // assuming the filename is passed as a URL parameter

	ctx := appengine.NewContext(c.Request)

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile("sduverse-04ef4993e58d.json"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}
	defer storageClient.Close()

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}
	defer f.Close()

	sw := storageClient.Bucket(bucket).Object(uploadedFile.Filename).NewWriter(ctx)

	if _, err := io.Copy(sw, f); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	if err := sw.Close(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "file updated successfully",
	})
}
