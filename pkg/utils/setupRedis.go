package utils

import (
	"fmt"
	"github.com/go-redis/redis/v8"
	"golang.org/x/net/context"
	"log"
)

func ConnectRedis(ctx context.Context, redisURI string, redisDB int) (*redis.Client, error) {
	redisClient := redis.NewClient(&redis.Options{
		Addr: redisURI,
		DB:   redisDB,
	})

	_, err := redisClient.Ping(ctx).Result()
	if err != nil {
		log.Println("Failed to connect to Redis:", err)
		return nil, fmt.Errorf("failed to connect to Redis, %w", err)
	}
	log.Println("Connected to Redis")
	return redisClient, nil
}
