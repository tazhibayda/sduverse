package main

import (
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"golang.org/x/net/context"
	"log"
	"os"
	"sduverse/config"
	_ "sduverse/docs"
	"sduverse/internal/handlers"
	"sduverse/internal/routes"
	"sduverse/pkg/auth"
	"sduverse/pkg/utils"
)

var conf config.Config

func setupConf() {
	conf = config.Config{
		Username: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		Host:     os.Getenv("DB_HOST"),
		Database: os.Getenv("DB_DATABASE"),
		RedisURI: os.Getenv("REDIS_URI"),
	}
}

//		@title			SDUVerse API
//		@version		1.0
//		@description	SDUVerse app api
//		@BasePath	/
//	 @securityDefinitions.apikey ApiKeyAuth
//	 @in header
//	 @name Authorization
func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatalf("err loading: %v", err)
	}
	setupConf()

	db := utils.DBSetup(conf)
	defer utils.DisconnectDB()

	redis, err := utils.ConnectRedis(context.Background(), conf.RedisURI, 1)
	if err != nil {
		panic("cant connect to Redis client")
	}
	utils.Router = utils.SetupRouter()

	logger := utils.Init()
	utils.InitializeDB(db.Client(), &logger)

	storage := utils.InitStorage("sduverse-04ef4993e58d.json")
	//routes.SetupRoutes(utils.Router, db)

	postRepository := handlers.NewPostRepository(db.Collection("posts"), storage, &logger)
	commentRepository := handlers.NewCommentRepository(db.Collection("comments"), storage, &logger)
	userRepository := handlers.NewUserRepository(db.Collection("users"), storage, &logger)
	authRepository := auth.NewAuthRepository(db.Collection("users"), storage, redis, &logger, "secret")
	serviceRepository := handlers.NewServiceRepository(db.Collection("services"), storage, &logger)
	booksRepository := handlers.NewServiceRepository(db.Collection("books"), storage, &logger)
	housingRepository := handlers.NewServiceRepository(db.Collection("housing"), storage, &logger)
	lostRepository := handlers.NewServiceRepository(db.Collection("lostandfound"), storage, &logger)

	routes.PostRoutes(utils.Router.Group("posts"), *postRepository)
	routes.CommentRoutes(utils.Router.Group("comments"), *commentRepository)
	routes.UserRoutes(utils.Router.Group("users"), *userRepository)
	routes.AuthRoutes(utils.Router.Group("auth"), *authRepository)
	routes.ServiceRoutes(utils.Router.Group("services"), *serviceRepository)
	routes.BooksRoutes(utils.Router.Group("services"), *booksRepository)
	routes.HousingRoutes(utils.Router.Group("services"), *housingRepository)
	routes.LostAndFoundRoutes(utils.Router.Group("services"), *lostRepository)

	utils.Router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	utils.Router.Use(gin.Recovery())

	utils.Router.POST("/upload", utils.HandleFileUploadToBucket)
	utils.Router.DELETE("/delete/:filename", utils.HandleFileDeleteFromBucket)
	utils.Router.PUT("/update", utils.HandleFileUpdateInBucket)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	defer func(Router *gin.Engine, addr ...string) {
		err := Router.Run(addr[0])
		if err != nil {
			log.Fatal("server error:", err)
		}
	}(utils.Router, ":"+port)
}
