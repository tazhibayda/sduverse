package test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"sduverse/internal/models"
	"sduverse/pkg/utils"
	"testing"
)

func TestGetAllUsers(t *testing.T) {
	router := utils.Router

	srv := httptest.NewServer(router)
	defer srv.Close()

	resp, err := http.Get("http://127.0.0.1:8080" + "/users")
	if err != nil {
		t.Fatalf("Failed to perform GET request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code: got %d, want %d", resp.StatusCode, http.StatusOK)
	}

}

func TestUserByID(t *testing.T) {
	resp, err := http.Get("http://127.0.0.1:8080" + "/users/1")
	if err != nil {
		t.Fatalf("Failed to perform GET request: %v", err)
	}
	defer resp.Body.Close()
	response := &models.User{}
	json.NewDecoder(resp.Body).Decode(&response)
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Unexpected status code: got %d, want %d", resp.StatusCode, http.StatusOK)
	}

	if response.ID != 1 {
		t.Errorf("Unexpected id of user: got %d, want %d", resp.StatusCode, http.StatusOK)
	}
	if response.Username != "example_username" {
		t.Errorf("Unexpected username of user: got %d, want %d", resp.StatusCode, http.StatusOK)
	}
	if response.Role != "example_role" {
		t.Errorf("Unexpected role of user: got %d, want %d", resp.StatusCode, http.StatusOK)
	}
}
