package config

type Config struct {
	Username string
	Password string
	Host     string
	Database string
	RedisURI string
}
