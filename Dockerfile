FROM golang:1.21.1

WORKDIR /go/src

ENV GOPATH /go

COPY ./ .

RUN go build -o sduverse ./cmd

RUN chmod +x ./sduverse

EXPOSE 8080

CMD ["./sduverse"]
