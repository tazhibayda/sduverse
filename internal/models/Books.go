package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Books struct {
	ID_       primitive.ObjectID `bson:"_id"`
	Title     string             `bson:"title"`
	Author    string             `bson:"author"`
	Year      int                `bson:"year"`
	Publisher string             `bson:"publisher"`
	Genre     string             `bson:"genre"`
	Language  string             `bson:"language"`
	ImageUrl  string             `bson:"image_url"`
	BookUrl   string             `bson:"bookUrl"`
	CreatedAt primitive.DateTime `bson:"createdAt"`
}
