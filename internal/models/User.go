package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//swagger:model
type User struct {
	ID_       primitive.ObjectID `json:"ID_" bson:"_id"`
	Username  string             `json:"username" bson:"username" form:"username"`
	Password  string             `json:"password" bson:"password" form:"password"`
	Name      string             `json:"name" bson:"name" form:"name"`
	Surname   string             `json:"surname" bson:"surname" form:"surname"`
	ImageUrl  string             `json:"imageUrl" bson:"image_url"`
	Email     string             `json:"email" bson:"email" form:"email"`
	Birthday  primitive.DateTime `json:"-" bson:"birthday" form:"birthday"`
	CreatedAt primitive.DateTime `json:"-" bson:"created_at" `
	UpdatedAt primitive.DateTime `json:"-" bson:"updated_at" `
}

//func (u *User) Create() {
//	userCollection := utils.DB.Collection("users")
//	u.ID_ = primitive.NewObjectID()
//	_, err := userCollection.InsertOne(context.Background(), *u)
//	if err != nil {
//		fmt.Errorf("create user error")
//		return
//	}
//}
