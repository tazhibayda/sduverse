package models

import "go.mongodb.org/mongo-driver/bson/primitive"

//swagger:model
type LostAndFoundItem struct {
	ID_       primitive.ObjectID `bson:"_id"`
	Status    string             `bson:"status"`
	Author    primitive.ObjectID `bson:"author"`
	ImageUrl  string             `bson:"image_url"`
	Contact   string             `bson:"contact"`
	CreatedAt primitive.DateTime `bson:"created_at"`
}
