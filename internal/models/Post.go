package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//swagger:model
type Post struct {
	ID_       primitive.ObjectID `bson:"_id" swaggertype:"primitive,integer"`
	Title     string             `bson:"title"`
	ImageUrl  string             `bson:"imageUrl"`
	Body      string             `bson:"body"`
	UserID    primitive.ObjectID `bson:"user_id"`
	Status    string             `bson:"status"`
	CreatedAt primitive.DateTime `json:"-" bson:"createdAt"`
	UpdatedAt primitive.DateTime `json:"-" bson:"updatedAt"`
}
