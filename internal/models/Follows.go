package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//swagger:model
type Following struct {
	UserID      primitive.ObjectID `bson:"user_id" bson:"user_id"`
	FollowingID primitive.ObjectID `bson:"following_id" bson:"following_id"`
	CreatedAt   primitive.DateTime `json:"-" bson:"created_at"`
}

//
//func (f *Following) Create() {
//	_, err := utils.DB.Collection("followings").InsertOne(context.Background(), f)
//	if err != nil {
//		fmt.Errorf("create follow error")
//		return
//	}
//}
