package models

//swagger:model
type Dormitory struct {
	Name    string `json:"name"`
	Course  int    `json:"course"`
	Faculty string `json:"faculty"`
	Gender  string `json:"gender"`
}
