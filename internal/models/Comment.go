package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//swagger:model
type Comment struct {
	ID_       primitive.ObjectID `bson:"_id"`
	PostID    primitive.ObjectID `bson:"post_id"`
	Body      string             `bson:"body"`
	ImageUrl  string             `bson:"image_url"`
	UserID    primitive.ObjectID `bson:"user_id"`
	CreatedAt primitive.DateTime `json:"-" bson:"created_at"`
}
