package models

import "go.mongodb.org/mongo-driver/bson/primitive"

//swagger:model
type Housing struct {
	ID_       primitive.ObjectID `bson:"_id"`
	Author    primitive.ObjectID `json:"author"`
	Address   string             `json:"address"`
	ImageUrl  string             `json:"image_url"`
	Contact   string             `bson:"contact"`
	CreatedAt primitive.DateTime `json:"createdAt"`
}
