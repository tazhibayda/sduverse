package routes

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"sduverse/internal/handlers"
	"sduverse/pkg/auth"
)

func AuthRoutes(router *gin.RouterGroup, repository auth.AuthRepository) {
	authRequired := router.Group("/auth", repository.AuthMiddleware())
	router.POST("/refresh-token", repository.RefreshToken)
	//protectedRoutes := router.Group("/api")
	//protectedRoutes.Use(repository.AuthMiddleware())
	//{
	authRequired.GET("/protected", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "You are authenticated!"})
	})
	//}
	router.POST("/signup", repository.SignUp)
	router.POST("/login", repository.Login)
}

func CommentRoutes(router *gin.RouterGroup, repository handlers.CommentRepository) {

	router.GET("/:id", repository.GetCommentByID)
	router.GET("/user/:user_id", repository.GetCommentsByUserID)
	router.GET("/post/:post_id", repository.GetCommentsByPostID)

	router.GET("/", repository.GetAllComments)

	router.PUT("/:id", repository.UpdateComment)

	router.DELETE("/:id", repository.DeleteComment)

	router.POST("/", repository.CreateComment)

}

func PostRoutes(router *gin.RouterGroup, repository handlers.PostRepository) {

	router.GET("/:id", repository.GetPostByID)

	router.GET("/", repository.GetAllPosts)

	router.PUT("/:id", repository.UpdatePost)

	router.DELETE("/:id", repository.DeletePost)

	router.POST("/", repository.CreatePost)

	router.POST("/post", repository.PostingNews)

}

func UserRoutes(router *gin.RouterGroup, repository handlers.UserRepository) {

	router.GET("/:id", repository.GetUserByID)

	router.GET("/", repository.GetAllUsers)

	router.PUT("/:id", repository.UpdateUser)

	router.DELETE("/:id", repository.DeleteUser)

	router.POST("/", repository.CreateUser)

	router.POST("/image", repository.UserImageCreate)

	router.POST("/:id/follow/:fid", repository.FollowToUser)

	router.GET("/:id/followers", repository.Followers)

	router.GET("/:id/follows", repository.Follows)
}

func ServiceRoutes(router *gin.RouterGroup, repository handlers.ServicesRepository) {

	router.POST("/dormitory", repository.DormitoryHandler)

}

func BooksRoutes(router *gin.RouterGroup, repository handlers.ServicesRepository) {
	router.POST("/books", repository.PostBook)
	router.GET("/books", repository.GetAllBooks)
	router.GET("/books/:id", repository.GetBookByID)
}
func HousingRoutes(router *gin.RouterGroup, repository handlers.ServicesRepository) {
	router.POST("/housing", repository.PostHousing)
	router.GET("/housing", repository.GetAllHousings)
	router.GET("/housing/:id", repository.GetHousingByID)
}
func LostAndFoundRoutes(router *gin.RouterGroup, repository handlers.ServicesRepository) {
	router.POST("/lostandfound", repository.PostLostAndFound)
	router.GET("/lostandfound", repository.GetAllLostAndFounds)
	router.GET("/lostandfound/:id", repository.GetLostAndFoundByID)
}
