package handlers

import (
	"cloud.google.com/go/storage"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"io"
	"net/http"
	"net/url"
	"sduverse/internal/models"
	"time"
)

type CommentRepository struct {
	repository *mongo.Collection
	storage    *storage.Client
	logger     *logrus.Logger
}

func NewCommentRepository(repository *mongo.Collection, client *storage.Client, logger *logrus.Logger) *CommentRepository {
	return &CommentRepository{
		repository: repository,
		storage:    client,
		logger:     logger,
	}
}

// GetCommentByID @Summary Get a comment by ID
// @Description Get a comment by its ID
//
//	@Tags			comments
//
// @Produce json
// @Param id path string true "Comment ID"
// @Success 200 {object} models.Comment
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /comments/{id} [get]
func (repository *CommentRepository) GetCommentByID(c *gin.Context) {
	//var commentCollection = utils.DB.Collection("comments")

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}
	var comment models.Comment
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&comment)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch comment"})
		return
	}
	c.JSON(http.StatusOK, comment)
}

// GetCommentsByUserID @Summary Get comments by user ID
// @Description Get comments by user ID
// @Produce json
//
//	@Tags			comments
//
// @Param user_id path int true "User ID"
// @Success 200 {array} models.Comment
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /comments/users/{user_id} [get]
func (repository *CommentRepository) GetCommentsByUserID(c *gin.Context) {
	//var commentCollection = utils.DB.Collection("comments")
	userId, err := primitive.ObjectIDFromHex(c.Param("user_id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}
	var comment models.Comment
	cursor, err := repository.repository.Find(context.Background(), bson.M{"user_id": userId})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch comment by user Id"})
		return
	}
	defer cursor.Close(context.Background())

	var comments []models.Comment
	for cursor.Next(context.Background()) {
		if err := cursor.Decode(&comment); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode comments"})
			return
		}
		comments = append(comments, comment)
	}
	if comments != nil {
		c.JSON(http.StatusOK, comments)
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user id"})
}

// GetCommentsByPostID @Summary Get comments by post ID
// @Description Get comments by post ID
//
//	@Tags			comments
//
// @Produce json
// @Param post_id path int true "Post ID"
// @Success 200 {array} models.Comment
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /comments/posts/{post_id} [get]
func (repository *CommentRepository) GetCommentsByPostID(c *gin.Context) {
	postId, err := primitive.ObjectIDFromHex(c.Param("post_id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}
	cursor, err := repository.repository.Find(context.Background(), bson.M{"post_id": postId})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch comment by post Id"})
		return
	}
	defer cursor.Close(context.Background())

	var comments []models.Comment
	for cursor.Next(context.Background()) {
		var comment models.Comment
		if err := cursor.Decode(&comment); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode comments"})
			return
		}
		comments = append(comments, comment)
	}
	if comments != nil {
		c.JSON(http.StatusOK, comments)
		return
	}
	c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post Id"})
}

// GetAllComments @Summary Get all comments
// @Description Get all comments
//
//	@Tags			comments
//
// @Produce json
// @Success 200 {array} models.Comment
// @Failure 500 {object} models.ErrorResponse
// @Router /comments [get]
func (repository *CommentRepository) GetAllComments(c *gin.Context) {
	var comments []models.Comment
	cursor, err := repository.repository.Find(context.Background(), bson.M{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch comments"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var comment models.Comment
		if err := cursor.Decode(&comment); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode posts"})
			return
		}
		comments = append(comments, comment)
	}
	if err := cursor.Err(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to iterate cursor"})
		return
	}
	c.JSON(http.StatusOK, comments)
}

// UpdateComment @Summary Update a comment by ID
// @Description Update a comment by its ID
//
//	@Tags			comments
//
// @Accept json
// @Produce json
// @Param id path string true "Comment ID"
// @Param input body models.Comment true "Updated comment details"
// @Success 200
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /comments/{id} [put]
func (repository *CommentRepository) UpdateComment(c *gin.Context) {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid comment ID"})
		return
	}

	var comment models.Comment
	if err := c.BindJSON(&comment); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid comment data"})
		return
	}

	_, err = repository.repository.UpdateOne(
		context.Background(),
		bson.M{"_id": id},
		bson.M{"$set": bson.M{
			"post_id":    comment.PostID,
			"body":       comment.Body,
			"user_id":    comment.UserID,
			"created_at": comment.CreatedAt,
		}},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update comment"})
		return
	}
	c.Status(http.StatusOK)
}

// DeleteComment @Summary Delete a comment by ID
// @Description Delete a comment by its ID
// @Param id path string true "Comment ID"
//
//	@Tags			comments
//
// @Success 200
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /comments/{id} [delete]
func (repository *CommentRepository) DeleteComment(c *gin.Context) {
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}

	_, err = repository.repository.DeleteOne(context.Background(), bson.M{"_id": id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete comment"})
		return
	}
	c.Status(http.StatusOK)
}

// CreateComment @Summary Create a new comment
// @Description Create a new comment
//
//	@Tags			comments
//
// @Accept multipart/form-data
// @Produce json
// @Param input body models.Comment true "Comment details"
// @Success 201 {object} models.Comment
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /comments [post]
func (repository *CommentRepository) CreateComment(c *gin.Context) {
	var comment models.Comment
	userId, err := primitive.ObjectIDFromHex(c.Request.FormValue("user_id"))
	hasImage := true
	comment.UserID = userId
	comment.Body = c.Request.FormValue("body")
	comment.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	comment.ID_ = primitive.NewObjectID()
	postID, err := primitive.ObjectIDFromHex(c.Request.FormValue("post_id"))
	comment.PostID = postID

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		if err == http.ErrMissingFile {
			hasImage = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
	}
	if hasImage {
		bucket := "verse_1"
		sw := repository.storage.Bucket(bucket).Object("comments/" + uploadedFile.Filename).NewWriter(context.Background())

		if _, err := io.Copy(sw, f); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err := sw.Close(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
		//name :=  user.ID_
		u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
		}
		comment.ImageUrl = "https://storage.googleapis.com" + u.RequestURI()
	}
	result, err := repository.repository.InsertOne(context.Background(), comment)
	if err != nil {

		repository.logger.Errorf("Failed to create post: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"err": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "path": comment.ImageUrl})
}
