package handlers

import (
	"cloud.google.com/go/storage"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"sduverse/internal/models"
	"time"
)

type PostRepository struct {
	postCollection *mongo.Collection
	storage        *storage.Client
	logger         *logrus.Logger
}

func NewPostRepository(repository *mongo.Collection, storage *storage.Client, logger *logrus.Logger) *PostRepository {
	return &PostRepository{repository, storage, logger}
}

// GetPostByID @Summary Get a post by ID
//
//	@Description	Get a post by its ID
//	@Produce		json
//	@Tags			posts
//	@Param			id	path		string	true	"Post ID"
//	@Success		200	{object}	models.Post
//	@Failure		400	{object}	models.ErrorResponse
//	@Router			/posts/{id} [get]
func (repository *PostRepository) GetPostByID(c *gin.Context) {
	param := c.Param("id")
	id, err := primitive.ObjectIDFromHex(param)
	if err != nil {
		repository.logger.Errorf("Faied to bind JSON: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}
	var post models.Post
	err = repository.postCollection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(&post)
	if err != nil {
		repository.logger.Errorf("Failed to fetch post: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch post"})
		return
	}
	c.JSON(http.StatusOK, post)
}

// GetAllPosts @Summary Get all posts
// @Description Get all posts
// @Produce json
//
//	@Tags			posts
//
// @Success 200 {array} models.Post
// @Failure 500 {object} models.ErrorResponse
// @Router /posts [get]
func (repository *PostRepository) GetAllPosts(c *gin.Context) {
	var posts []models.Post
	cursor, err := repository.postCollection.Find(context.Background(), bson.M{})
	if err != nil {
		repository.logger.Errorf("Faied to bind JSON: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch posts"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var post models.Post
		if err := cursor.Decode(&post); err != nil {
			repository.logger.Errorf("Failed to decode posts: %v", err)
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode posts"})
			return
		}
		posts = append(posts, post)
	}
	if err := cursor.Err(); err != nil {
		repository.logger.Errorf("Failed to iterate cursor: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to iterate cursor"})
		return
	}
	c.JSON(http.StatusOK, posts)
}

// CreatePost @Summary Create a new post
// @Description Create a new post
// @Accept json
//
//	@Tags			posts
//
// @Produce json
// @Param input body models.Post true "Post details"
// @Success 201 {object} models.Post
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /posts [post]
func (repository *PostRepository) CreatePost(c *gin.Context) {
	var post models.Post
	if err := c.BindJSON(&post); err != nil {
		repository.logger.Errorf("Faied to bind JSON: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post data"})
		return
	}

	post.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	post.ID_ = primitive.NewObjectID()

	_, err := repository.postCollection.InsertOne(context.Background(), post)
	if err != nil {

		repository.logger.Errorf("Failed to create post: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"err": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "Post created successfully", "post": post})
}

// DeletePost @Summary Delete a post by ID
// @Description Delete a post by its ID
// @Param id path string true "Post ID"
//
//	@Tags			posts
//
// @Success 200
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /posts/{id} [delete]
func (repository *PostRepository) DeletePost(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		repository.logger.Errorf("Faied to bind JSON: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}

	deleteResult, err := repository.postCollection.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil || deleteResult.DeletedCount == 0 {
		repository.logger.Errorf("Failed to delete post: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete post"})
		return
	}
	c.Status(http.StatusOK)
}

// UpdatePost @Summary Update a post by ID
// @Description Update a post by its ID
// @Accept json
// @Produce json
//
//	@Tags			posts
//
// @Param id path int true "Post ID"
// @Param input body models.Post true "Updated post details"
// @Success 200
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /posts/{id} [put]
func (repository *PostRepository) UpdatePost(c *gin.Context) {
	//postCollection = utils.DB.Collection("posts")
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		repository.logger.Errorf("Faied to bind JSON: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}

	var post models.Post
	if err := c.BindJSON(&post); err != nil {
		repository.logger.Errorf("Failed to update post: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post data"})
		return
	}

	_, err = repository.postCollection.UpdateOne(
		context.Background(),
		bson.M{"_id": id},
		bson.M{"$set": bson.M{
			"title":      post.Title,
			"body":       post.Body,
			"user_id":    post.UserID,
			"status":     post.Status,
			"created_at": post.CreatedAt,
		}},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update post"})
		return
	}
	c.Status(http.StatusOK)
}

// PostingNews @Summary Create a post
// @Description Create a new user with image
// @Accept multipart/form-data
// @Produce json
//
//	@Tags			posts
//
// @Param input body models.Post true "Posting news"
// @Success 201 {object} models.User
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /posts/post [post]
func (repository *PostRepository) PostingNews(c *gin.Context) {

	var post models.Post
	hasImage := true

	id, err := primitive.ObjectIDFromHex(c.Request.FormValue("user_id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "invalid user id",
		})
		return
	}

	post.UserID = id
	post.Body = c.Request.FormValue("body")
	post.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	post.ID_ = primitive.NewObjectID()
	post.Title = c.Request.FormValue("title")
	post.Status = "active"

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		if err == http.ErrMissingFile {
			hasImage = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
	}
	if hasImage {
		bucket := "verse_1"
		fileExtension := filepath.Ext(uploadedFile.Filename)
		newFileName := post.ID_.Hex() + fileExtension
		sw := repository.storage.Bucket(bucket).Object("posts/" + newFileName).NewWriter(context.Background())

		if _, err := io.Copy(sw, f); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err := sw.Close(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
		//name :=  user.ID_
		u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
		}
		post.ImageUrl = "https://storage.googleapis.com" + u.RequestURI()
	}
	result, err := repository.postCollection.InsertOne(context.Background(), post)
	if err != nil {

		repository.logger.Errorf("Failed to create post: %v", err)
		c.JSON(http.StatusInternalServerError, gin.H{"err": err.Error()})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "path": post.ImageUrl})

}
