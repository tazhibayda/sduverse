package handlers

import (
	"cloud.google.com/go/storage"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"sduverse/internal/models"
	"strconv"
	"time"
)

type ServicesRepository struct {
	repository *mongo.Collection
	storage    *storage.Client
	logger     *logrus.Logger
}

func NewServiceRepository(repository *mongo.Collection, storageClient *storage.Client, logger *logrus.Logger) *ServicesRepository {
	return &ServicesRepository{repository: repository, storage: storageClient, logger: logger}
}

// DormitoryHandler @Summary Create a new dormitory
// @Description Create a new dormitory
// @Accept json
// @Produce json
//
//	@Tags			services
//
// @Param input body models.Dormitory true "Dormitory details"
// @Success 201 {object} models.Dormitory
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/dormitory [post]
func (repository *ServicesRepository) DormitoryHandler(c *gin.Context) {
	var dorm models.Dormitory
	err := c.BindJSON(&dorm)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch dorm object"})
		return
	}

	result, err := repository.repository.InsertOne(context.Background(), dorm)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create dorm"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "dorm": dorm})
}

// PostLostAndFound @Summary Create a new LostAndFound Item
// @Description Create a new dormitory
// @Accept multipart/form-data
// @Produce json
//
//	@Tags			services
//
// @Param input body models.LostAndFoundItem true "Lost And Found details"
// @Success 201 {object} models.LostAndFoundItem
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/lostandfound [post]
func (repository *ServicesRepository) PostLostAndFound(c *gin.Context) {
	var item models.LostAndFoundItem
	id, err := primitive.ObjectIDFromHex(c.Request.FormValue("user_id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "atoi",
		})
	}
	hasImage := true
	item.ID_ = primitive.NewObjectID()
	item.Author = id
	item.Contact = c.Request.FormValue("contact")
	item.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	item.Status = c.Request.FormValue("status")

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		if err == http.ErrMissingFile {
			hasImage = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
	}
	if hasImage {
		bucket := "verse_1"
		sw := repository.storage.Bucket(bucket).Object("lostandfound/" + uploadedFile.Filename).NewWriter(context.Background())

		if _, err := io.Copy(sw, f); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err := sw.Close(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
		}
		item.ImageUrl = "https://storage.googleapis.com" + u.RequestURI()
	}
	result, err := repository.repository.InsertOne(context.Background(), item)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create dorm"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "image": item.ImageUrl})
}

// GetAllLostAndFounds @Summary Get all LostAndFound items
// @Description Create a new dormitory
// @Produce json
//
//	@Tags			services
//
// @Success 201 {object} models.LostAndFoundItem
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/lostandfound [get]
func (repository *ServicesRepository) GetAllLostAndFounds(c *gin.Context) {
	var items []models.LostAndFoundItem
	cursor, err := repository.repository.Find(context.Background(), bson.M{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch items"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var item models.LostAndFoundItem
		if err := cursor.Decode(&item); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode items"})
			return
		}
		items = append(items, item)
	}
	if err := cursor.Err(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to iterate cursor"})
		return
	}
	c.JSON(http.StatusOK, items)
}

// GetLostAndFoundByID @Summary Get LostAndFound item by id
// @Description Get LostAndFound item by id
// @Produce json
//
//	@Tags			services
//	@Param			id	path		string	true	"Item ID"
//
// @Success 201 {object} models.LostAndFoundItem
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/lostandfound/{id} [get]
func (repository *ServicesRepository) GetLostAndFoundByID(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}
	var item models.LostAndFoundItem
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&item)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch comment"})
		return
	}
	c.JSON(http.StatusOK, item)
}

// PostHousing @Summary Create a new Housing
// @Description Create a new housing
// @Accept multipart/form-data
// @Produce json
//
//	@Tags			services
//
// @Param input body models.Housing true "LostAndFoundItem details"
// @Success 201 {object} models.Housing
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/housing [post]
func (repository *ServicesRepository) PostHousing(c *gin.Context) {
	var housing models.Housing
	id, err := primitive.ObjectIDFromHex(c.Request.FormValue("user_id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "atoi",
		})
	}
	hasImage := true

	housing.ID_ = primitive.NewObjectID()
	housing.Author = id
	housing.Contact = c.Request.FormValue("contact")
	housing.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	housing.Address = c.Request.FormValue("address")

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		if err == http.ErrMissingFile {
			hasImage = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
	}
	if hasImage {
		bucket := "verse_1"
		sw := repository.storage.Bucket(bucket).Object("housing/" + uploadedFile.Filename).NewWriter(context.Background())

		if _, err := io.Copy(sw, f); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err := sw.Close(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
		}
		housing.ImageUrl = "https://storage.googleapis.com" + u.RequestURI()
	}
	result, err := repository.repository.InsertOne(context.Background(), housing)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create dorm"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "image": housing.ImageUrl})
}

// GetAllHousings @Summary Get all LostAndFound items
// @Description Create a new dormitory
// @Produce json
// @Tags			services
// @Success 201 {object} models.Housing
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/housing [get]
func (repository *ServicesRepository) GetAllHousings(c *gin.Context) {
	var housings []models.Housing
	cursor, err := repository.repository.Find(context.Background(), bson.M{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch items"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var housing models.Housing
		if err := cursor.Decode(&housing); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode items"})
			return
		}
		housings = append(housings, housing)
	}
	if err := cursor.Err(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to iterate cursor"})
		return
	}
	c.JSON(http.StatusOK, housings)
}

// GetHousingByID @Summary Get all LostAndFound items
// @Description Create a new dormitory
// @Produce json
// @Tags			services
//
//	@Param			id	path		string	true	"Item ID"
//
// @Success 201 {object} models.Housing
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/housing/{id} [get]
func (repository *ServicesRepository) GetHousingByID(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}
	var housing models.Housing
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&housing)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch comment"})
		return
	}
	c.JSON(http.StatusOK, housing)
}

// PostBook @Summary Create a new Book
// @Description Create a new book
// @Accept multipart/form-data
// @Produce json
//
//	@Tags			services
//
// @Param input body models.Books true "LostAndFoundItem details"
// @Success 201 {object} models.Books
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/books [post]
func (repository *ServicesRepository) PostBook(c *gin.Context) {
	var book models.Books

	hasImage := true

	year, err := strconv.Atoi(c.Request.FormValue("year"))

	book.ID_ = primitive.NewObjectID()
	book.Title = c.Request.FormValue("title")
	book.Author = c.Request.FormValue("author")
	book.Year = year
	book.Publisher = c.Request.FormValue("publisher")
	book.Genre = c.Request.FormValue("genre")
	book.Language = c.Request.FormValue("language")
	book.CreatedAt = primitive.NewDateTimeFromTime(time.Now())

	f, uploadedImage, err := c.Request.FormFile("image")
	if err != nil {
		if err == http.ErrMissingFile {
			hasImage = false
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
	}

	bookFile, uploadedBook, err := c.Request.FormFile("book")
	if err != nil {
		if err == http.ErrMissingFile {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": "book hasn't uploaded",
				"error":   true,
			})
			return
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}
	}

	if hasImage {
		bucket := "verse_1"
		fileExtension := filepath.Ext(uploadedImage.Filename)
		newFileName := book.ID_.Hex() + fileExtension
		sw := repository.storage.Bucket(bucket).Object("books/" + newFileName).NewWriter(context.Background())

		if _, err := io.Copy(sw, f); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err := sw.Close(); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
			return
		}

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
				"error":   true,
			})
		}
		book.ImageUrl = "https://storage.googleapis.com" + u.RequestURI()
	}

	bucket := "verse_1"
	fileExtension := filepath.Ext(uploadedBook.Filename)
	newFileName := book.ID_.Hex() + fileExtension
	sw := repository.storage.Bucket(bucket).Object("books/" + newFileName).NewWriter(context.Background())

	if _, err := io.Copy(sw, bookFile); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	if err := sw.Close(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
	}
	book.BookUrl = "https://storage.googleapis.com" + u.RequestURI()

	result, err := repository.repository.InsertOne(context.Background(), book)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create dorm"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "image": book.ImageUrl, "book": book.BookUrl})
}

// GetAllBooks @Summary Get all Books
// @Description  Get all Books
// @Produce json
// @Tags			services
// @Success 201 {object} models.Housing
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/books [get]
func (repository *ServicesRepository) GetAllBooks(c *gin.Context) {
	var books []models.Books
	cursor, err := repository.repository.Find(context.Background(), bson.M{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch items"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var book models.Books
		if err := cursor.Decode(&book); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode items"})
			return
		}
		books = append(books, book)
	}
	if err := cursor.Err(); err != nil {
		repository.logger.Error("error", "Failed to fetch books")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to iterate cursor"})
		return
	}
	repository.logger.Info("books", "successfully get all")
	c.JSON(http.StatusOK, gin.H{"book": books})
}

// GetBookByID @Summary Get book by id
// @Description Get book by id
// @Produce json
// @Tags			services
//
//	@Param			id	path		string	true	"Item ID"
//
// @Success 201 {object} models.Housing
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /services/books/{id} [get]
func (repository *ServicesRepository) GetBookByID(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}
	var book models.Books
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&book)
	if err != nil {
		repository.logger.Error("error", "Failed to fetch book")
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch book"})
		return
	}
	repository.logger.Info("books", "successfully get by id")
	c.JSON(http.StatusOK, gin.H{"book": book})
}
