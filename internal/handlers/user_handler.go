package handlers

import (
	"cloud.google.com/go/storage"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/net/context"
	"io"
	"net/http"
	"net/url"
	"sduverse/internal/models"
	"time"
)

type UserRepository struct {
	repository *mongo.Collection
	storage    *storage.Client
	logger     *logrus.Logger
}

func NewUserRepository(repository *mongo.Collection, storageClient *storage.Client, logger *logrus.Logger) *UserRepository {
	return &UserRepository{repository: repository, storage: storageClient, logger: logger}
}

// GetAllUsers @Summary Get all users
// @Description Get all users
//
//	@Tags			users
//
// @Produce json
// @Success 200 {array} models.User
// @Failure 500 {object} models.ErrorResponse
// @Router /users [get]
func (repository *UserRepository) GetAllUsers(c *gin.Context) {
	var users []models.User
	cursor, err := repository.repository.Find(context.Background(), bson.M{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch users"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var user models.User
		if err := cursor.Decode(&user); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode users"})
			return
		}
		users = append(users, user)
	}
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch user"})
		return
	}
	c.JSON(http.StatusOK, users)

}

// GetUserByID @Summary Get a user by ID
// @Description Get a user by its ID
// @Produce json
//
//	@Tags			users
//
// @Param id path string true "User ID"
// @Success 200 {object} models.User
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/{id} [get]
func (repository *UserRepository) GetUserByID(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}
	var user models.User
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch user"})
		return
	}
	c.JSON(http.StatusOK, user)
}

// UpdateUser @Summary Update a user by ID
// @Description Update a user by its ID
// @Accept json
//
//	@Tags			users
//
// @Produce json
// @Param id path string true "User ID"
// @Param input body models.User true "Updated user details"
// @Success 200
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/{id} [put]
func (repository *UserRepository) UpdateUser(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	var user models.User
	if err := c.BindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user data"})
		return
	}
	_, err = repository.repository.UpdateOne(
		context.Background(),
		bson.M{"_id": id},
		bson.M{"$set": bson.M{
			"username":   user.Username,
			"updated_at": time.Now(),
		}},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update user"})
		return
	}
	c.Status(http.StatusOK)
}

// DeleteUser @Summary Delete a user by ID
// @Description Delete a user by its ID
// @Param id path string true "User ID"
// @Success 200
//
//	@Tags			users
//
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/{id} [delete]
func (repository *UserRepository) DeleteUser(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}
	deleteResult, err := repository.repository.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil || deleteResult.DeletedCount == 0 {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete user"})
		return
	}

	c.Status(http.StatusOK)
}

// UserImageCreate @Summary Create a new user with image
// @Description Create a new user with image
// @Accept multipart/form-data
// @Produce json
//
//	@Tags			users
//
// @Param input body models.User true "User details"
// @Success 201 {object} models.User
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/image [post]
func (repository *UserRepository) UserImageCreate(c *gin.Context) {
	var user models.User

	id, err := primitive.ObjectIDFromHex(c.Request.FormValue("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": "atoi",
		})
		return
	}

	f, uploadedFile, err := c.Request.FormFile("file")
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	bucket := "verse_1"
	sw := repository.storage.Bucket(bucket).Object("avatars/" + uploadedFile.Filename).NewWriter(context.Background())

	if _, err := io.Copy(sw, f); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	if err := sw.Close(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}
	//name :=  user.ID_
	u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
			"error":   true,
		})
	}
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&user)
	url := "https://storage.googleapis.com" + u.RequestURI()

	result, err := repository.repository.UpdateOne(
		context.Background(),
		bson.M{"_id": id},
		bson.M{"$set": bson.M{
			"image_url": url,
		}},
	)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create user"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.ModifiedCount, "user": u.Path})
}

// CreateUser @Summary Create a new user
// @Description Create a new user
// @Accept json
// @Produce json
//
//	@Tags			users
//
// @Param input body models.User true "User details"
// @Success 201 {object} models.User
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users [post]
func (repository *UserRepository) CreateUser(c *gin.Context) {
	var user models.User
	if err := c.BindJSON(&user); err != nil {
		fmt.Println(user)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user data"})
		return
	}
	user.ID_ = primitive.NewObjectID()
	user.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	user.UpdatedAt = primitive.NewDateTimeFromTime(time.Now())

	result, err := repository.repository.InsertOne(context.Background(), user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create user"})
		return
	}

	c.JSON(http.StatusCreated, gin.H{"id": result.InsertedID, "user": user})
}

// FollowToUser @Summary Follow a user
// @Description Follow a user
// @Accept json
//
//	@Tags			users
//
// @Produce json
// @Param id path string true "User ID"
// @Param fid path string true "Following user ID"
// @Success 201 {object} models.Following
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/{id}/follow/{fid} [post]
func (repository *UserRepository) FollowToUser(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	var user *models.User
	user = nil
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": id}).Decode(&user)

	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	fid, err := primitive.ObjectIDFromHex(c.Param("fid"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user follow ID"})
		return
	}
	user = nil
	err = repository.repository.FindOne(context.Background(), bson.M{"_id": fid}).Decode(&user)

	if user == nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid following user ID"})
		return
	}

	var follow models.Following

	err = repository.repository.FindOne(context.Background(), bson.M{"user_id": id, "following_id": fid}).Decode(&follow)
	if err != mongo.ErrNoDocuments {
		c.JSON(http.StatusOK, gin.H{"exists": "User already followed"})
		return
	}

	follow.UserID = id
	follow.FollowingID = fid
	follow.CreatedAt = primitive.NewDateTimeFromTime(time.Now())

	_, err = repository.repository.InsertOne(context.Background(), follow)
	if err != nil {
		fmt.Errorf("create user error")
		return
	}

	c.JSON(http.StatusCreated, follow)
}

// Followers @Summary Get followers of a user
// @Description Get followers of a user
// @Produce json
//
//	@Tags			users
//
// @Param id path string true "User ID"
// @Success 200 {array} models.User
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/{id}/followers [get]
func (repository *UserRepository) Followers(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	var users []models.User
	cursor, err := repository.repository.Find(context.Background(), bson.M{"following_id": id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch users"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var follow models.Following
		if err := cursor.Decode(&follow); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var user models.User
		if err := repository.repository.FindOne(context.Background(), bson.M{"_id": follow.UserID}).Decode(&user); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode users"})
			return
		}
		users = append(users, user)
	}

	c.JSON(http.StatusOK, users)
}

// Follows @Summary Get users followed by a user
// @Description Get users followed by a user
// @Produce json
//
//	@Tags			users
//
// @Param id path string true "User ID"
// @Success 200 {array} models.User
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
// @Router /users/{id}/follows [get]
func (repository *UserRepository) Follows(c *gin.Context) {

	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	var users []models.User
	cursor, err := repository.repository.Find(context.Background(), bson.M{"user_id": id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch users"})
		return
	}
	defer cursor.Close(context.Background())
	for cursor.Next(context.Background()) {
		var follow models.Following
		if err := cursor.Decode(&follow); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		var user models.User
		if err := repository.repository.FindOne(context.Background(), bson.M{"_id": follow.FollowingID}).Decode(&user); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to decode users"})
			return
		}
		users = append(users, user)
	}

	c.JSON(http.StatusOK, users)
}
